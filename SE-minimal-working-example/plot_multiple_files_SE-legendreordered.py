# -*- coding: utf-8 -*-
"""
Created on Mon Nov 24 14:00:01 2014

@author: Andy
"""

import pylab


def plotSE(listOfFiles, referenceFile, plotTitle, pictureName):
    from mpldatacursor import datacursor
                      
    datalist = [ ( pylab.loadtxt(filename), label ) for filename, label in listOfFiles ]
    reference = pylab.loadtxt(referenceFile[0])
    pylab.figure()
    for data, label in datalist:
        # SE is always a positive value                
        lines = pylab.plot( data[:,0]/1e9, abs(data[:,1]-reference[:,1]), label=label)
        #lines = pylab.plot( data[:,0], abs(data[:,1]-reference[:,1]), label=label)
        datacursor(lines)  
    pylab.legend()
    #pylab.title("Title of Plot")
    #pylab.xlabel("X Axis Label")
    #pylab.ylabel("Y Axis Label")
    pylab.semilogx()
    pylab.grid(which='both')
    pylab.xlim(0.1,20)
    pylab.ylim(0,160)
    pylab.xlabel('Frequency (GHz)')
    pylab.ylabel('SE (dB)') 
    pylab.title(plotTitle)
    ax = pylab.gca()
    import matplotlib
    ax.xaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%d'))
    pylab.show()
    pylab.savefig(pictureName + '.png')
    pylab.title('')
    pylab.savefig(pictureName + '.eps')
    #pylab.savefig('..\\..\\..\\figures\\' + pictureName + '.eps')
    pylab.savefig(pictureName + '.eps')
    
    
if __name__ == '__main__':
    
    #reference = ('../../square_no_rings_no_bls_no_guardring','reference')
    reference = ('square_no_rings_no_bls_no_guardring','reference')    
       
    listOfFiles = [('0_vias_per_side.txt','0 vias'),
             ('2_vias_per_side.txt','2 vias, separation 39.5 mm'),
             ('3_vias_per_side.txt','3 vias, separation 19.5 mm'),
             ('5_vias_per_side.txt','5 vias, separation 9.5 mm'),
             ('9_vias_per_side.txt','9 vias, separation 4.5 mm'),
             ]    
    listOfFiles = reversed(listOfFiles)
    
    plotSE(listOfFiles,reference, 'Shielding Effectiveness in Function of Frequency', 'SE_wrt_vias_per_side')
 
